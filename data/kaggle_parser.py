from kaggle.api.kaggle_api_extended import KaggleApi


def load_kaggle_data():
    api = KaggleApi()
    api.authenticate()
    api.dataset_download_files(
        "tmdb/tmdb-movie-metadata", quiet=False, unzip=True
    )
