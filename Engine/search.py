import copy
import math
import pathlib
import random
import re
import string
from typing import Dict, List, Tuple

import nltk
import pandas as pd
from nltk import pos_tag
from nltk.corpus import stopwords, wordnet
from nltk.stem import WordNetLemmatizer
from nltk.stem.snowball import SnowballStemmer
from scipy.spatial.distance import cdist
from sklearn.feature_extraction.text import TfidfVectorizer

nltk.download("stopwords")
nltk.download("averaged_perceptron_tagger")
nltk.download("wordnet")
nltk.download("omw-1.4")
sw_eng = set(stopwords.words("english"))
stemmer = SnowballStemmer(language="english")


def get_wordnet_pos(pos_tag: str) -> str:
    """Определяем часть речи слова

    Args:
        pos_tag (str): часть речи

    Returns:
        str: часть речи из wordnet
    """
    if pos_tag.startswith("J"):
        return wordnet.ADJ
    elif pos_tag.startswith("V"):
        return wordnet.VERB
    elif pos_tag.startswith("N"):
        return wordnet.NOUN
    elif pos_tag.startswith("R"):
        return wordnet.ADV
    else:
        return wordnet.NOUN


def clean_text(start_text: str) -> str:
    """
    Общая функция очистки текста. Сначала делает буквы строчными, затем
    разделяет слова на токены по знакам пунктуации, затем убираются все цифры,
    стоп-слова и пустые токены из токенов. Определяем часть речи и по ней
    лемматизируем

    Args:
        text (str): изначальный текст в виде строки

    Returns:
        str: очищенный текст в виде строки
    """

    lower_text = start_text.lower()
    splitted_text = [
        word.strip(string.punctuation) for word in lower_text.split(" ")
    ]
    undigited_text = [
        word for word in splitted_text if not any(c.isdigit() for c in word)
    ]
    stop = stopwords.words("english")
    wo_stop_text = [x for x in undigited_text if x not in stop]
    wo_empty_tokens_text = [t for t in wo_stop_text if len(t) > 0]
    pos_tags = pos_tag(wo_empty_tokens_text)
    lemma_text = [
        WordNetLemmatizer().lemmatize(t[0], get_wordnet_pos(t[1]))
        for t in pos_tags
    ]
    tokens_text = [t for t in lemma_text if len(t) > 1]
    text = " ".join(tokens_text)

    return text


class Document:
    """
    Класс, в котором отображаются документы по запросу.
    Возвращает пару тайтл-текст, отформатированную под запрос
    """

    def __init__(self, title, text):
        self.title = title
        self.text = text

    def format(self, query):
        return [self.title, self.text]


# Считывает данные из датасета
def read_data(filepath: pathlib.Path) -> pd.DataFrame:
    """
    Возвращает нужные для обработки поля датафрейма

    Args:
        filepath (Path): путь до датафрейма с даткой

    Returns:
        data (DataFrame): нужные поля сырых данных
    """
    data = pd.read_csv(filepath)
    data = data[
        [
            "original_title",
            "overview",
        ]
    ]
    return data


def build_index(data: pd.DataFrame) -> Tuple[Dict, List]:
    """
    Cчитывает сырые данные и строит инвертированный индекс.
    Для каждого документа из raw_data и для каждого слова из очищенного
    набора слов описания - если слова нет в index, то создаем под него
    пару ключ-значения, где значение это индекс документа из raw_data. То же
    самое для названий. Это и есть инвертированный индекс.

    Args:
        data (DataFrame): сырые данные про фильмы

    Returns:
        index (dict) - инвертированный индекс
        raw_data (list) - список файлов типа document содержащий сырые данные
    """
    index: Dict[str, List[int]] = {}
    raw_data: List[Document] = []

    for i in range(len(data)):
        raw_data.append(
            Document(
                str(data.loc[i, "original_title"]),
                str(data.loc[i, "overview"]),
            )
        )

    for idx, doc in enumerate(raw_data):
        for word in (
            " ".join([word for word in doc.text.split() if word not in sw_eng])
        ).split():
            if word.lower() not in index:
                index[word.lower()] = []
            index[word.lower()].append(idx)
        for word in (
            " ".join(
                [word for word in doc.title.split() if word not in sw_eng]
            )
        ).split():
            if word.lower() not in index:
                index[word.lower()] = []
            index[word.lower()].append(idx)
    print("Количество ключей в инвертированном индексе = ", len(index.keys()))
    return index, raw_data


def score(query: str, document: Document) -> float:
    """
    Возвращает скор для пары запрос-документ.
    Больше -> Релевантнее

    Если очередь пустая - возвращаем случайный скор. Если нет - сначала очищаем
    текст, делаем TF-IDF для очереди и документа, используем косинусную
    cхожесть. Достаем название документа и убираем все лишнее из названия.
    Увеличиваем вес, если слово из очереди встречается в названии и отлавливаем
    НаНы.

    Args:
        query (str): запрос, поступивший из поисковика
        document (Document): корпус фильмов

    Returns:
        float: значение релевантности от 0 до 1
    """

    if query == "":
        return random.random()
    else:
        text = clean_text(document.text)
        vec = TfidfVectorizer()
        document_tfidf = vec.fit_transform([text]).todense()
        query_tfidf = vec.transform(query.split()).todense()
        rec_idx = 1.0 - cdist(query_tfidf, document_tfidf, "cosine")[0, 0]
        title = re.sub(r"[^a-zA-z\s]", "", document.title)
        for word in query.split():
            if word in title.lower():
                rec_idx = rec_idx + 0.5
        if math.isnan(rec_idx):
            rec_idx = 0.0
        return rec_idx


def retrieve(index: Dict, raw_data: List, query: str) -> List[Document]:
    """
    Возвращает начальный список релевантных документов
    В candidates записываем конечный набор документов, который
    будет выводиться на экран. sets будет служить двумерным списком, в
    котором будут сравниваться по значениям разные ключи (слова) словаря
    index. Через mid_raw_data вывод candidates по индексам raw_data. Если
    пустой запрос - пусть возвращает первые 100 документов сырых данных.
    Токенизируем запрос и достаем его длину. Если длина запроса - 1 слово,
    то в релевантные документы заносим все из списка по инвертированному
    индексу. Если нет - создаем массив нулей размером с длину очереди, Для
    каждого слова очереди в массив sets заносим копию списка документов
    (описаний), в которых это слово встречается, затем сравниваются списки
    индексированных статей для каждых ДВУХ слов запроса, пересечения
    записываются в candidates. Если пересечений так и не нашлось, возвращаем
    списки релевантных документов для каждого слова по отдельности. Печатаем,
    что пересечений не нашлось (для отладки). Превращаем массив индексов в
    массив документов. Выводим 100 документов.

    Args:
        index(Dict): инвертированный индекс для документов считанной датки
        raw_data (List): список документов
        query (str): запрос

    Returns:
        list[Document]: Cписок релевантных документов (100 шт)
    """

    candidates: list = []
    sets: list = []
    mid_raw_data: list = []

    if query == "":
        return raw_data[:100]

    list_query = query.split()

    indexed_words = [
        word for word in list_query if index.get(word.lower()) is not None
    ]
    n = len(indexed_words)

    if n == 0:
        return raw_data[:100]
    elif n == 1:
        candidates.extend(index[indexed_words[0].lower()])
    else:
        sets = [0] * n

        for word_idx, word in enumerate(indexed_words):
            if index.get(word.lower()) is not None:
                sets[word_idx] = copy.deepcopy(index[word.lower()])
            else:
                continue
            print("len(sets[{}]]) = ".format(word_idx), len(sets[word_idx]))

        for i in range(len(sets)):
            for j in range(i + 1, len(sets)):
                candidates.extend(list(set(sets[i]) & set(sets[j])))

        if len(candidates) <= 5:
            print("They have few or no shared candidates!!!")
            for i in range(n):
                candidates.extend(index[indexed_words[i].lower()])

    print("len(candidates) = ", len(candidates))
    for i in set(candidates):
        mid_raw_data.append(raw_data[i])

    return mid_raw_data[:100]
