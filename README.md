# Movie-search-engine

Запускается через server.py

Поисковик по фильмам "Eliajah", составленный на датасете в 5000 фильмов от The Movie Database (tmdb). Содержит подробнейшее описание последовательности действий при написании движка.

Запустить контейнер:

docker build -t search-engine .

docker run -p 127.0.0.1:5000:5000 -it search-engine