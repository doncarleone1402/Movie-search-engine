import errno
import os
from urllib import error
from urllib.request import urlopen

import data

# def test_tmdb():
#     api_key = os.environ.get("API_KEY_TMDB")
#     assert data.makedataset(api_key) == 2 or data.makedataset(api_key) == 0


# TODO: deal with 99
def test_url():
    category = data.CATEGORIES[0]
    id_list_name = data.make_category_id_url_suffix(category)
    raw_url = data.ID_LISTS_RAW_URL.format(id_list_name)
    bad_reasons = [
        "[Errno 99] Cannot assign requested address",
        "Service Unavailable",
    ]
    try:
        code = urlopen(raw_url).getcode()
        assert code == 200
    except error.URLError as err:
        err_code = err.errno
        err_reason = err.reason
        print("err_reason = ", str(err_reason))
        print("err_code = ", err_code)
        assert err_code == 503 or str(err_reason) in bad_reasons


# TODO: deal with 99
def test_url_api():
    api_key = os.environ.get("API_KEY_TMDB")
    category = data.CATEGORIES[0]
    entry_id = 550
    category_specifics = ""
    call_url = data.BASE_API_CALL.format(
        category=category,
        entry_id=entry_id,
        api_key=api_key,
        category_specifics=category_specifics,
    )
    bad_reasons = [errno.ECONNREFUSED, 99]
    try:
        code = urlopen(call_url).getcode()
        print("code = ", code)
        assert code == 200
    except error.URLError as err:
        reason = err.reason
        print("reason = ", reason)
        reason_errno = reason.errno
        print("reason.errno = ", reason_errno)
        assert reason_errno in bad_reasons
