from kaggle.api.kaggle_api_extended import KaggleApi


def test_data_kaggle():
    api = KaggleApi()
    api.authenticate()
    list_tmdb_data = api.datasets_list(search="TMDB 5000 Movie Dataset")
    assert len(list_tmdb_data) > 0
